# Copyright (c) Facebook, Inc. and its affiliates. All rights reserved.

import argparse
import os

class BaseOptions():
    def __init__(self):
        self.initialized = False
        self.parser = None

    def initialize(self, parser):
        # Datasets related
        g_data = parser.add_argument_group('Data')
        g_data.add_argument('--input_image', type=str, default='./sample_image/1.jpg', help='path to image')
        g_data.add_argument('--height', type=int, default=163, help='Real person height')
        g_data.add_argument('--save_all', type=str, default=None, help='save all inter files')
        g_data.add_argument('--output_path', type=str, default=None, help='.obj output path')
        g_data.add_argument('--output_name', type=str, default=None, help='.obj output name')
        g_data.add_argument('--loadSize', type=int, default=1024, help='load size of input image')
        

        g_model = parser.add_argument_group('Model')
        g_model.add_argument('--load_netMR_checkpoint_path', type=str, help='path to save checkpoints')

        g_test = parser.add_argument_group('Testing')
        g_test.add_argument('--resolution', type=int, default=512, help='# of grid in mesh reconstruction')
        
        # special tasks
        self.initialized = True
        return parser

    def gather_options(self, args=None):
        # initialize parser with basic options
        if not self.initialized:
            parser = argparse.ArgumentParser(
                formatter_class=argparse.ArgumentDefaultsHelpFormatter)
            parser = self.initialize(parser)
            self.parser = parser

        if args is None:
            return self.parser.parse_args()
        else:
            return self.parser.parse_args(args)

    def print_options(self, opt):
        message = ''
        message += '----------------- Options ---------------\n'
        for k, v in sorted(vars(opt).items()):
            comment = ''
            default = self.parser.get_default(k)
            if v != default:
                comment = '\t[default: %s]' % str(default)
            message += '{:>25}: {:<30}{}\n'.format(str(k), str(v), comment)
        message += '----------------- End -------------------'
        print(message)

    def parse(self, args=None):
        opt = self.gather_options(args)
        return opt
