import sys
import time
sys.path.insert(0, '..')
import json
import os
import cv2
import base64
import codecs
import pickle
import pandas as pd
import streamlit as st

from PIL import Image
from VideoCamera import VideoCamera
from generator import recon_from_array
from streamlit_lottie import st_lottie
from streamlit_lottie import st_lottie_spinner

st.set_page_config(
    page_title="Provador Virtual",
    page_icon=Image.open("logo.png"),
    layout="centered")

hide_menu_style = """
        <style>
        #MainMenu {visibility: hidden;}
        footer {visibility: hidden;}
        </style>
        """
st.markdown(hide_menu_style, unsafe_allow_html=True) 

def render_image(image):
    """Renders the given svg string."""
    return f'<img style="width:100%;padding:0 20% 20% 20%;" src="data:image/png;base64,{base64.b64encode(open(image, "rb").read()).decode()}">'

def check_password():
    st.sidebar.markdown(render_image('logo.png'), unsafe_allow_html=True)
    st.sidebar.markdown('Faça login utilizando o e-mail utilizado no cadastrado. Em seguida, tire a foto e aguarde o processamento.')
    st.sidebar.title('Login')
    st.sidebar.markdown('')
    input_email = st.sidebar.text_input('E-Mail', key="email")
    if not input_email:
        st.stop()
    else:
        if input_email.strip().lower() == 'admin': return True
        else:
            st.sidebar.error("😕 Verifique o E-Mail inserido")
            return False

def normalize_image(img, height, xpadding=230):
    (h, w) = img.shape[:2]
    window_height = height
    new_width = int(float(window_height)*float(w)/float(h))
    img = cv2.resize(img, (new_width, window_height), interpolation=cv2.INTER_LINEAR)
    img = cv2.copyMakeBorder(img, 0, 0, xpadding, xpadding, cv2.BORDER_CONSTANT, value=(14, 17, 23))
    return img

if 'camera' not in st.session_state:
    st.session_state.camera = True

if 'image' not in st.session_state:
    st.session_state.image = None

if 'data_confirm' not in st.session_state:
    st.session_state.data_confirm = False
    
if 'mesh_recon' not in st.session_state:
    st.session_state.mesh_recon = None
    
if 'mesh_recon_result' not in st.session_state:
    st.session_state.mesh_recon_result = None

scanner_json = json.load(open("./scanner.json", 'r'))
human_json = json.load(open("./human-scan.json", 'r'))
placeholder = st.empty()

def load_data():
    columns = ['Nome', 'Idade', 'Altura', 'Peso', 'Sexo']
    data = [['Sophia', '22', '163', '54', 'Feminino']]
    st.session_state.height = 163
    st.title('Informações')
    st.table(pd.DataFrame(columns=columns, data=data))
    
def data_confirm():
    st.session_state.data_confirm = True

def run_mesh_recon():
    st.session_state.mesh_recon = True

def return_camera():
    st.session_state.image = None
    st.session_state.mesh_recon = None
    st.session_state.camera = True

def main():
    placeholder.empty()
    with placeholder.container():
        st_lottie(human_json, key='Initial')
    if check_password():
        placeholder.empty()
        if st.session_state.data_confirm == False:
            with placeholder.container():
                load_data()
                _, col2 = st.columns([20,1])
                with col2:
                    st.button('Confirmar', on_click=data_confirm)
        
        else:
            if st.session_state.camera:
                st.session_state.camera = False
                camera = VideoCamera()
                
                placeholder.empty()
                with placeholder.container():
                    st.title('Captação de Imagem')
                    col1, col2 = st.columns([3,1])
                    with col1:
                        FRAME_WINDOW = st.image([])
                    with col2:
                        for _ in range(20): st.write('')
                        button = st.button('Tirar Foto', on_click=camera.turnoff)
                    while not button:
                        img = camera.get_frame(rotate=-90)
                        while img is None:
                            img = camera.get_frame(rotate=-90)
                        st.session_state.image = img.copy()
                        FRAME_WINDOW.image(normalize_image(img, 700, 100))
            
        if (st.session_state.image is not None) and (st.session_state.mesh_recon is None):
            placeholder.empty()
            with placeholder.container():
                st.title('Imagem')      
                st.image(normalize_image(st.session_state.image, 700))
                st.markdown('Agora iremos processar a sua imagem. Após finalizarmos, os dados ficarão disponíveis na plataforma onde você se cadastrou.')
                col1, _, col2 = st.columns([1,20,1])
                with col1:
                    st.button('Voltar', on_click=return_camera)
                    
                with col2:
                    st.button('Confirmar', on_click=run_mesh_recon)
        
        if st.session_state.mesh_recon is not None:
            if st.session_state.mesh_recon_result is None:
                placeholder.empty()
                with st_lottie_spinner(scanner_json, key='Final'):
                    obj_base64string = codecs.encode(pickle.dumps(st.session_state.image, protocol=pickle.HIGHEST_PROTOCOL), "base64").decode('latin1')
                    cmd = ['--input_image', obj_base64string, '--load_netMR_checkpoint_path', f'..{os.sep}models{os.sep}pifuhd.pt',
                           '--output_path', f'..{os.sep}results']
                    recon_from_array(cmd)
                    st.session_state.mesh_recon_result = True
                    
            if st.session_state.mesh_recon_result is not None:
                placeholder.empty()
                with placeholder.container():
                    st.info('A malha foi gerada!')
                    time.sleep(10)
                    st.session_state.camera = True
                    st.session_state.image = None
                    st.session_state.data_confirm = False
                    st.session_state.mesh_recon = None
                    st.session_state.mesh_recon_result = None                
      
if __name__ == '__main__':
    main()