import os
import torch
import pickle
import codecs
import trimesh
import numpy as np

from datetime import datetime
from src.utils import drop_artefacts
from lib.options import BaseOptions
from src.image_processing import EvalImage
from lib.model import HGPIFuNetwNML, HGPIFuMRNet
from lib.mesh_util import string_obj_mesh, reconstruction

def gen_mesh(res, net, device, data, thresh=0.5, use_octree=True, components=False):
    image_tensor_global = data['img_512'].to(device=device)
    image_tensor = data['img'].to(device=device)
    calib_tensor = data['calib'].to(device=device)

    net.filter_global(image_tensor_global)
    net.filter_local(image_tensor[:,None])

    try:
        if net.netG.netF is not None:
            image_tensor_global = torch.cat([image_tensor_global, net.netG.nmlF], 0)
        if net.netG.netB is not None:
            image_tensor_global = torch.cat([image_tensor_global, net.netG.nmlB], 0)
    except:
        pass
    
    try:
        verts, faces, _, _ = reconstruction(
            net, device, calib_tensor, res, thresh, use_octree=use_octree, num_samples=50000)
        verts_tensor = torch.from_numpy(verts.T).unsqueeze(0).to(device=device).float()

        color = np.zeros(verts.shape)
        interval = 50000
        for i in range(len(color) // interval + 1):
            left = i * interval
            if i == len(color) // interval:
                right = -1
            else:
                right = (i + 1) * interval
            net.calc_normal(verts_tensor[:, None, :, left:right], calib_tensor[:,None], calib_tensor)
            nml = net.nmls.detach().cpu().numpy()[0] * 0.5 + 0.5
            color[left:right] = nml.T

        return string_obj_mesh(verts, faces, color)
    except Exception as e:
        print(e)

def extend_opt(func):
    def wrapper_func(args=None):
        opt = BaseOptions().parse(args)
        return func(opt)
    return wrapper_func

@extend_opt
def recon(opt):
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    state_dict = torch.load(opt.load_netMR_checkpoint_path, map_location=device)
    input_image = opt.input_image
    resolution = opt.resolution
    output_path = opt.output_path
    loadSize = opt.loadSize
    outputName = opt.output_name
    
    opt = state_dict['opt']
    opt.input_image = input_image
    opt.resolution = resolution
    opt.output_path = output_path
    opt.loadSize = loadSize
    opt.output_name = outputName
    
    img = EvalImage.from_path(opt, opt.input_image)

    netG = HGPIFuNetwNML(state_dict['opt_netG'], projection_mode='orthogonal').to(device=device)
    netMR = HGPIFuMRNet(opt, netG, projection_mode='orthogonal').to(device=device)
    
    def set_eval():
        netG.eval()
    
    netMR.load_state_dict(state_dict['model_state_dict'])
    
    with torch.no_grad():
        set_eval()
        string_obj_mesh = gen_mesh(opt.resolution, netMR, device, img.dict, components=opt.use_compose)
    
    try: os.mkdir(opt.output_path)
    except: pass

    loaded_mesh = trimesh.load(file_obj = trimesh.util.wrap_as_stream(string_obj_mesh), file_type='obj')
    loaded_mesh = drop_artefacts(loaded_mesh)
    name = datetime.now().strftime('%Y%m%d-%H%M%S') if (opt.output_name == 'None') or (opt.output_name is None) else opt.output_name
    loaded_mesh.export(f'{opt.output_path}{os.sep}{name}.obj')
    
@extend_opt
def recon_from_array(opt):
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    state_dict = torch.load(opt.load_netMR_checkpoint_path, map_location=device)
    input_image = opt.input_image
    resolution = opt.resolution
    output_path = opt.output_path
    loadSize = opt.loadSize
    outputName = opt.output_name
    height = opt.height
    save_all = opt.save_all
    
    opt = state_dict['opt']
    opt.input_image = input_image
    opt.resolution = resolution
    opt.output_path = output_path
    opt.loadSize = loadSize
    opt.output_name = outputName
    opt.height = height
    opt.save_all = save_all
    
    img = pickle.loads(codecs.decode(opt.input_image.encode('latin1'), "base64"))
    img = EvalImage(opt, img)

    netG = HGPIFuNetwNML(state_dict['opt_netG'], projection_mode='orthogonal').to(device=device)
    netMR = HGPIFuMRNet(opt, netG, projection_mode='orthogonal').to(device=device)
    
    def set_eval():
        netG.eval()
    
    netMR.load_state_dict(state_dict['model_state_dict'])
    
    with torch.no_grad():
        set_eval()
        string_obj_mesh = gen_mesh(opt.resolution, netMR, device, img.dict, components=opt.use_compose)
    
    try: os.mkdir(opt.output_path)
    except: pass
 
    loaded_mesh = trimesh.load(file_obj = trimesh.util.wrap_as_stream(string_obj_mesh), file_type='obj')
    loaded_mesh = drop_artefacts(loaded_mesh)
    name = datetime.now().strftime('%Y%m%d-%H%M%S') if (opt.output_name == 'None') or (opt.output_name is None) else opt.output_name
    loaded_mesh.export(f"{opt.output_path}{os.sep}{name}.obj")

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input_image', type=str, default=f'.{os.sep}sample_images{os.sep}1.jpg')
    parser.add_argument('--height', type=int, default=163)
    parser.add_argument('-s', '--save_all', type=bool, default=False)
    parser.add_argument('-o', '--output_path', type=str, default=f'.{os.sep}results')
    parser.add_argument('-n', '--output_name', type=str, default=None)
    parser.add_argument('-r', '--resolution', type=int, default=200)
    parser.add_argument('-l', '--load_netMR_checkpoint_path', type=str, default=f'.{os.sep}models{os.sep}pifuhd.pt')
    args = parser.parse_args()

    ###############################################################################################
    ##                   Upper PIFu
    ###############################################################################################

    height = str(args.height)
    res = str(args.resolution)
    cmd = ['--input_image', args.input_image, '--height', height, '--save_all', args.save_all,
           '--output_path', args.output_path, '--resolution', res, '--load_netMR_checkpoint_path',
            args.load_netMR_checkpoint_path, '--output_name', args.output_name]
    recon(cmd)