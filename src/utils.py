import numpy as np

def select_closest(mslice, pos:np.ndarray):
    size = len(mslice.entities)
    centroids = list()
    for i in range(size):
        idxs = list(range(size))
        idxs.remove(i)
        msliceTemp = mslice.copy()
        msliceTemp.remove_entities(idxs)
        centroids.append(msliceTemp.centroid)

    dist = np.linalg.norm(pos-centroids, axis=1)
    idxs = list(range(size))
    idxs.remove(np.argmin(dist))
    mslice.remove_entities(idxs)
    return mslice

def drop_artefacts(mesh):
    cc = mesh.split(only_watertight=False)    
    out_mesh = cc[0]
    bbox = out_mesh.bounds
    height = bbox[1,0] - bbox[0,0]
    for c in cc:
        bbox = c.bounds
        if height < bbox[1,0] - bbox[0,0]:
            height = bbox[1,0] - bbox[0,0]
            out_mesh = c
    
    return out_mesh