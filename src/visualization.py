import trimesh
import numpy as np

angle = 0
fps = 30 

def rotate(scene):
    global angle, fps
    
    angle += (2*np.pi)/(20 * fps)
    R = trimesh.transformations.rotation_matrix(angle, [0,1,0])
    
    node = s.graph.nodes_geometry[0]
    scene.graph.update(node, matrix=R)

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input_mesh', type=str, default='results/1_400.obj')
    args = parser.parse_args()
    
    mesh = trimesh.load_mesh(args.input_mesh)
    s = trimesh.Scene([mesh])
    
    s.show(callback=rotate, callback_period = 1. / fps)